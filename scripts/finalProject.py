# SCRIPT FOR ECE 383 FINAL PROJECT (UR5 GOLF)
# CREATED BY SYDNEY HUNT 
# DUE 12-09-2022


# IMPORT PACKAGES
from __future__ import print_function
from shutil import move
from six.moves import input
import sys
import copy
import rospy
import moveit_msgs.msg
import  moveit_commander
import geometry_msgs.msg


# IMPORT MATH SYMBOLS
try:
    from math import pi, tau, dist, fabs, cos
except: # do this for Python 2 compatibility
    from math import pi, fabs, cos, sqrt 
    tau = 2.0 * pi
    def dist(p,q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


# DEFINE FUNCTION FOR (6) JOINTS ON ROBOT
# This function will be used to tell the robot where to go instead of tying jointGoal[0] = ... for each
def goToThisJointState(moveGroup, joint0, joint1, joint2, joint3, joint4, joint5):
    # We want to be at proper position for all 6 joints
    jointGoal = moveGroup.get_current_joint_values()

    # 6 joints 
    jointGoal[0] = joint0
    jointGoal[1] = joint1
    jointGoal[2] = joint2
    jointGoal[3] = joint3
    jointGoal[4] = joint4
    jointGoal[5] = joint5

    # Need to begin trajectory
    moveGroup.go(jointGoal, wait = True)

    # Need to end trajectory
    moveGroup.stop()


# DEFINE STARTING POSITION FUNCTION
# The robot will move to this "reset" position before beginning each putt
def newPuttResetPosition(moveGroup):
    # Move robot to starting position (arbitrarily chose this point--only significance is "robot's hand" (end effector) is relatively center)
    # 18, -86, 86, 0, 18, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 18*pi/180, -86*pi/180, 86*pi/180, 0, 18*pi/180, 0) # don't forget to convert to radians!


# DEFINE ROBOT TRAJECTORY
def main():

# INITIALIZATION SETUP
    # Initialize  moveit_commander
    moveit_commander.roscpp_initialize(sys.argv)

    # Initialize a rospy node
    rospy.init_node("midterm_project", anonymous=True)

    # Instantiate a RobotCommander object
    robot = moveit_commander.RobotCommander()

    # Instantiate a MoveGroupCommander object
    groupName = "manipulator"
    moveGroup = moveit_commander.MoveGroupCommander(groupName)

# RESET
    # Go to starting position because we are about to do a new putt 
    #newPuttResetPosition(moveGroup) # removed to simplify trajectory

# STRAIGHT LINE
    # Move closer to robot to start straight line using joint angles
    # 0, -110, 110, 0, 0, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, -31*pi/180, -60*pi/180, 117*pi/180, -57*pi/180, -31*pi/180, 1) # don't forget to convert to radians!

    # Move away from robot to end straight line using joint angles
    # 0, -82, 82, 0, 0, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 28*pi/180, -50*pi/180, 97*pi/180, -47*pi/180, 28*pi/180, 1) # don't forget to convert to radians!
  

# RUN THIS SCRIPT
if __name__ == "__main__":
    main()
