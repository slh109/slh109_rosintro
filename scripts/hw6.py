# Script for ECE 383 HW 6 Question 3
# Created by Sydney Hunt on 10-09-2022


# IMPORT PACKAGES
from __future__ import print_function
from shutil import move
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg


# IMPORT MATH SYMBOLS
try:
    from math import pi, tau, dist, fabs, cos
except: # needs to be compatible with Python 2
    from math import pi, fabs, cos, sqrt 
    tau = 2.0 * pi
    def dist(p,q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


# DEFINE (6) JOINTS ON ROBOT
def go_to_joint_state(move_group, joint_0, joint_1, joint_2, joint_3, joint_4, joint_5):
    # Goal is to be at proper position for all 6 joints
    joint_goal = move_group.get_current_joint_values()

    # 6 joints because 6 DOF on robot
    joint_goal[0] = joint_0
    joint_goal[1] = joint_1
    joint_goal[2] = joint_2
    joint_goal[3] = joint_3
    joint_goal[4] = joint_4
    joint_goal[5] = joint_5

    # Need to delay trajectory
    move_group.go(joint_goal, wait = True)

    # Need to end trajectory
    move_group.stop()


# DEFINE ROBOT TRAJECTORY
def main():
    # Initialize moveit_commander
    moveit_commander.roscpp_initialize(sys.argv)

    # Initialize a rospy node
    rospy.init_node("midterm_project", anonymous=True)

    # Instantiate a RobotCommander object
    robot = moveit_commander.RobotCommander()

    # Instantiate a MoveGroupCommander object
    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    # Move to position 1/4 on letter "S" using joint angles
    # 22, -94, 92, 0, 22, 0 degrees (found from manually moving robot to desired position and looking at joint angles)
    go_to_joint_state(move_group, 11*pi/90, -47*pi/90, 23*pi/45, 0, 11*pi/90, 0) # don't forget to convert to radians!

    # Move to position 2/4 on letter "S" using joint angles
    # 22, -60, 75, -15, 22, 0 degrees (found from manually moving robot to desired position and looking at joint angles)
    go_to_joint_state(move_group, 11*pi/90, -pi/3, 5*pi/12, -pi/12, 11*pi/90, 0) # don't forget to convert to radians!

    # Move to position 3/4 on letter "S" using joint angles
    # 45, -75, 122, -45, 45, 0 degrees (found from manually moving robot to desired position and looking at joint angles)
    go_to_joint_state(move_group, pi/4, -5*pi/12, 61*pi/90, -pi/4, pi/4, 0) # don't forget to convert to radians!

    # Move to position 4/4 on letter "S" using joint angles
    # 30, -45, 82, -45, 30, 0 degrees (found from manually moving robot to desired position and looking at joint angles)
    go_to_joint_state(move_group, pi/6, -pi/4, 41*pi/90, -pi/4, pi/6, 0) # don't forget to convert to radians!


# RUN THIS SCRIPT
if __name__ == "__main__":
    main()
