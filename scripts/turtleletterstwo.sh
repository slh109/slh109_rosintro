rosservice call /turtle1/teleport_absolute 4 5 8.5

rosservice call /spawn 8 5 5 "turtle2"
rosservice call /turtle2/teleport_absolute 8 1.5 7.5
rosservice call /turtle2/set_pen 255 0 0 5 0
rosservice call clear


rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist  -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 4.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist  -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, -4.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist  -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist  -- '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist  -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist  -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'


