# SCRIPT FOR ECE 383 MIDTERM PROJECT
# CREATED BY SYDNEY HUNT 
# DUE 10-17-2022


# IMPORT PACKAGES
from __future__ import print_function
from shutil import move
from six.moves import input
import sys
import copy
import rospy
import moveit_msgs.msg
import  moveit_commander
import geometry_msgs.msg


# IMPORT MATH SYMBOLS
try:
    from math import pi, tau, dist, fabs, cos
except: # do this for Python 2 compatibility
    from math import pi, fabs, cos, sqrt 
    tau = 2.0 * pi
    def dist(p,q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


# DEFINE FUNCTION FOR (6) JOINTS ON ROBOT
# This function will be used to tell the robot where to go instead of tying jointGoal[0] = ... for each
def goToThisJointState(moveGroup, joint0, joint1, joint2, joint3, joint4, joint5):
    # We want to be at proper position for all 6 joints
    jointGoal = moveGroup.get_current_joint_values()

    # 6 joints 
    jointGoal[0] = joint0
    jointGoal[1] = joint1
    jointGoal[2] = joint2
    jointGoal[3] = joint3
    jointGoal[4] = joint4
    jointGoal[5] = joint5

    # Need to begin trajectory
    moveGroup.go(jointGoal, wait = True)

    # Need to end trajectory
    moveGroup.stop()


# DEFINE STARTING POSITION FUNCTION
# The robot will move to this "reset" position before beginning each letter
def newLetterResetPosition(moveGroup):
    # Move robot to starting position (arbitrarily chose this point--only significance is "robot's hand" (end effector) is relatively center)
    # 18, -86, 86, 0, 18, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 18*pi/180, -86*pi/180, 86*pi/180, 0, 18*pi/180, 0) # don't forget to convert to radians!


# DEFINE ROBOT TRAJECTORY
def main():

# INITIALIZATION SETUP
    # Initialize  moveit_commander
    moveit_commander.roscpp_initialize(sys.argv)

    # Initialize a rospy node
    rospy.init_node("midterm_project", anonymous=True)

    # Instantiate a RobotCommander object
    robot = moveit_commander.RobotCommander()

    # Instantiate a MoveGroupCommander object
    groupName = "manipulator"
    moveGroup = moveit_commander.MoveGroupCommander(groupName)


# RESET
    # Go to starting position because we are about to draw a new letter
    newLetterResetPosition(moveGroup) # function previously defined above


# LETTER S (curvy)
    # Move to position 1/10 on letter "S" using joint angles
    # 15, -102, 73, 30, 15, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 15*pi/180, -102*pi/180, 73*pi/180, 30*pi/180, 15*pi/180, 0) # don't forget to convert to radians!

    # Move to position 2/10 on letter "S" using joint angles
    # 8, -87, 75, 12, 8, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 8*pi/180, -87*pi/180, 75*pi/180, 12*pi/180, 8*pi/180, 0) # don't forget to convert to radians!

    # Move to position 3/10 on letter "S" using joint angles
    # 8, -77, 75, 2, 8, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 8*pi/180, -77*pi/180, 75*pi/180, 2*pi/180, 8*pi/180, 0) # don't forget to convert to radians!

    # Move to position 4/10 on letter "S" using joint angles
    # 17, -82, 86, -4, 17, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 17*pi/180, -82*pi/180, 86*pi/180, -4*pi/180, 17*pi/180, 0) # don't forget to convert to radians!

    # Move to position 5/10 on letter "S" using joint angles
    # 28, -94, 105, -11, 28, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 28*pi/180, -94*pi/180, 105*pi/180, -11*pi/180, 28*pi/180, 0) # don't forget to convert to radians!

    # Move to position 6/10 on letter "S" using joint angles
    # 36, -94, 115, -21, 36, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 36*pi/180, -94*pi/180, 115*pi/180, -21*pi/180, 36*pi/180, 0) # don't forget to convert to radians!

    # Move to position 7/10 on letter "S" using joint angles
    # 40, -88, 121, -33, 40, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 40*pi/180, -88*pi/180, 121*pi/180, -33*pi/180, 40*pi/180, 0) # don't forget to convert to radians!

    # Move to position 8/10 on letter "S" using joint angles
    # 34, -76, 118, -41, 34, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 34*pi/180, -76*pi/180, 118*pi/180, -41*pi/180, 34*pi/180, 0) # don't forget to convert to radians!

    # Move to position 9/10 on letter "S" using joint angles
    # 22, -62, 98, -36, 22, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 22*pi/180, -62*pi/180, 98*pi/180, -36*pi/180, 22*pi/180, 0) # don't forget to convert to radians!

    # Move to position 10/10 on letter "S" using joint angles
    # 16, -63, 82, 019, 16, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 16*pi/180, -63*pi/180, 82*pi/180, -19*pi/180, 16*pi/180, 0) # don't forget to convert to radians!


# LETTER S (pointy)
    # Move to position 1/4 on letter "S" using joint angles
    # 22, -94, 92, 0, 22, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    #goToThisJointState(moveGroup, 11*pi/90, -47*pi/90, 23*pi/45, 0, 11*pi/90, 0) # don't forget to convert to radians!

    # Move to position 2/4 on letter "S" using joint angles
    # 22, -60, 75, -15, 22, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    #goToThisJointState(moveGroup, 11*pi/90, -pi/3, 5*pi/12, -pi/12, 11*pi/90, 0) # don't forget to convert to radians!

    # Move to position 3/4 on letter "S" using joint angles
    # 45, -75, 122, -45, 45, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    #goToThisJointState(moveGroup, pi/4, -5*pi/12, 61*pi/90, -pi/4, pi/4, 0) # don't forget to convert to radians!

    # Move to position 4/4 on letter "S" using joint angles
    # 30, -45, 82, -45, 30, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    #goToThisJointState(moveGroup, pi/6, -pi/4, 41*pi/90, -pi/4, pi/6, 0) # don't forget to convert to radians!


# RESET
    # Go to starting position because we are about to draw a new letter
    newLetterResetPosition(moveGroup) # function previously defined above


# LETTER L
    # Move to position 1/3 on letter "L" using joint angles
    # -9, -75, 60, 15, -9, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, -9*pi/180, -5*pi/12, pi/3, pi/12, -9*pi/180, 0) # don't forget to convert to radians!

     # Move to position 2/3 on letter "L" using joint angles
    # 23, -62, 98, -36, 23, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 23*pi/180, -62*pi/180, 98*pi/180, -36*pi/180, 23*pi/180, 0) # don't forget to convert to radians!
  
    # Move to position 3/3 on letter "L" using joint angles
    # 39, -79, 122, -43, 39, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 39*pi/180, -79*pi/180, 122*pi/180, -43*pi/180, 39*pi/180, 0) # don't forget to convert to radians!


# RESET
    # Go to starting position because we are about to draw a new letter
    newLetterResetPosition(moveGroup) # function previously defined above


# LETTER H
    # Move to position 1/7 on letter "H" using joint angles
    # 32, -103, 102, 1, 32, -1 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 32*pi/180, -103*pi/180, 102*pi/180, 1*pi/180, 32*pi/180, -1*pi/180) # don't forget to convert to radians!

    # Move to position 2/7 on letter "H" using joint angles
    # 39, -79, 122, -43, 39, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 39*pi/180, -79*pi/180, 122*pi/180, -43*pi/180, 39*pi/180, 0) # don't forget to convert to radians!

    # Move to position 3/7 on letter "H" using joint angles
    # 33, 097, 86, 11, 33, 1 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 33*pi/180, -97*pi/180, 86*pi/180, 11*pi/180, 33*pi/180, 1*pi/180) # don't forget to convert to radians!
    
    # Move to position 4/7 on letter "H" using joint angles
    # 0, -110, 110, 0, 0, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 0, -110*pi/180, 110*pi/180, 0, 0, 0) # don't forget to convert to radians!

    # Move to position 5/7 on letter "H" using joint angles
    # 0, -82, 82, 0, 0, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 0, -82*pi/180, 82*pi/180, 0, 0, 0) # don't forget to convert to radians!

    # Move to position 6/7 on letter "H" using joint angles
    # 15, -60, 110, -60, 15, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 15*pi/180, -60*pi/180, 110*pi/180, -60*pi/180, 15*pi/180, 0) # don't forget to convert to radians!

    # Move to position 7/7 on letter "H" using joint angles
    # 19, -76, 57, 18, 19, 0 degrees 
    # (degrees found from manually moving robot to desired position and looking at joint angles)
    goToThisJointState(moveGroup, 19*pi/180, -76*pi/180, 57*pi/180, 18*pi/180, 19*pi/180, 0) # don't forget to convert to radians!
    
# RESET
    # Go to starting position because we are ending the program
    newLetterResetPosition(moveGroup) # function previously defined above


# RUN THIS SCRIPT
if __name__ == "__main__":
    main()
